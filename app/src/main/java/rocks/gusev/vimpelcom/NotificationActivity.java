package rocks.gusev.vimpelcom;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class NotificationActivity extends AppCompatActivity {

    private RelativeLayout mRootLayout;
    private LinearLayout mRevealLayout;
    private RelativeLayout.LayoutParams mParams = null;
    private int mCollapsed;
    private int mExpanded;
    private VelocityTracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        mRootLayout = (RelativeLayout) findViewById(R.id.rlRoot);
        mRevealLayout = (LinearLayout) findViewById(R.id.llNotification);

        mCollapsed = (int) getResources().getDimension(R.dimen.reveal_layout_min_size);
        mExpanded = (int) getResources().getDimension(R.dimen.reveal_layout_max_size);

        // Get layout params for future use
        if (mRevealLayout != null) {
            mRevealLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        mRevealLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mRevealLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    mParams = (RelativeLayout.LayoutParams) mRevealLayout.getLayoutParams();
                }
            });
            mRevealLayout.setOnTouchListener(mOnTouchListener);
        }

        // Set button actions
        Button btnOne = (Button) findViewById(R.id.btnOne);
        if (btnOne != null) {
            btnOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("CLICK", "BUTTON 1");
                }
            });
        }
        Button btnTwo = (Button) findViewById(R.id.btnTwo);
        if (btnTwo != null) {
            btnTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("CLICK", "BUTTON 2");
                }
            });
        }
    }


    /**
     * Here we track gestures and velocity, calculating how much to reveal when user swipes or just
     * moves the finger
     */
    View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        int mStartY;
        int mHeightAtStart;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (mParams == null) {
                return (false);
            }
            int pointerId = event.getPointerId(event.getActionIndex());
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    if(mTracker == null) {
                        // Retrieve a new VelocityTracker object to watch the velocity of a motion.
                        mTracker = VelocityTracker.obtain();
                    } else {
                        mTracker.clear();
                    }
                    mTracker.addMovement(event);
                    mStartY = Y;
                    mHeightAtStart = mParams.height;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mStartY = 0;
                    mHeightAtStart = 0;

                    float velocity = VelocityTrackerCompat.getYVelocity(mTracker, pointerId);
                    int middle = (mCollapsed + mExpanded) / 2;
                    int calculatedFinalPosition = (mParams.height > middle) ? mExpanded : mCollapsed;
                    if (velocity < -1000) {
                        calculatedFinalPosition = mCollapsed;
                    } else if (velocity > 1000) {
                        calculatedFinalPosition = mExpanded;
                    }
                    final int finalPosition = calculatedFinalPosition;
                    final int initialHeight = mParams.height;

                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
                            if (finalPosition == mExpanded) {
                                mParams.height = (interpolatedTime == 1) ? finalPosition : initialHeight + (int)(Math.abs(finalPosition - initialHeight) * interpolatedTime);
                            } else {
                                mParams.height = initialHeight - (int)(Math.abs(finalPosition - initialHeight) * interpolatedTime);
                            }
                            mRevealLayout.setLayoutParams(mParams);
                            mRevealLayout.requestLayout();
                        }

                        @Override
                        public boolean willChangeBounds() {
                            return (true);
                        }
                    };
                    a.setDuration((int)(Math.abs(finalPosition - initialHeight) / getResources().getDisplayMetrics().density));
                    mRevealLayout.startAnimation(a);

                    break;
                case MotionEvent.ACTION_MOVE:
                    mTracker.addMovement(event);
                    // When you want to determine the velocity, call
                    // computeCurrentVelocity(). Then call getXVelocity()
                    // and getYVelocity() to retrieve the velocity for each pointer ID.
                    mTracker.computeCurrentVelocity(1000);
                    int mYDelta = Y - mStartY;
                    if ((mHeightAtStart + mYDelta < mCollapsed) || (mHeightAtStart + mYDelta > mExpanded)) {
                        break;
                    }
                    mParams.height = mHeightAtStart + mYDelta;
                    mRevealLayout.setLayoutParams(mParams);
                    break;
            }
            mRootLayout.invalidate();
            return (true);
        }
    };
}
