package rocks.gusev.vimpelcom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnAction = (Button) findViewById(R.id.btnAction);
        if (btnAction != null) {
            btnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNextActivity();
                }
            });
        }
    }

    private void startNextActivity() {
        startActivity(new Intent(this, NotificationActivity.class));
    }
}
